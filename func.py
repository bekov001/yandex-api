import requests

from variables import notches


def get_map(x, y, scale, view):
    params = {
        "ll": f"{x},{y}",
        "spn": f"{scale},{scale}",
        "l": view

    }
    if notches:
        params["pt"] = "~".join(
            [",".join(map(str, el)) for el in notches])
    # response = requests.get(api, params=params)
    map_request = f"http://static-maps.yandex.ru/1.x/"
    response = requests.get(map_request, params=params)

    if not response:
        print("Ошибка выполнения запроса:")
        print(map_request, x, y, scale)
        print("Http статус:", response.status_code, "(", response.reason,
              ")")
        exit(1)
    map_file = f"map.png"
    with open(map_file, "wb") as file:
        file.write(response.content)
    return map_file


def get_info(name):
    """Получает координаты по имени"""
    geocoder_api_server = "http://geocode-maps.yandex.ru/1.x/"

    geocoder_params = {
        "apikey": "40d1649f-0493-4b70-98ba-98533de7710b",
        "geocode": name,
        "format": "json"
    }

    response = requests.get(geocoder_api_server, params=geocoder_params)

    if not response:
        # обработка ошибочной ситуации
        pass

    # Преобразуем ответ в json-объект
    json_response = response.json()
    # Получаем первый топоним из ответа геокодера.
    toponym = json_response["response"]["GeoObjectCollection"][
        "featureMember"][0]["GeoObject"]
    # Координаты центра топонима:
    address = toponym["metaDataProperty"]["GeocoderMetaData"]["text"]
    toponym_coodrinates = toponym["Point"]["pos"]
    post_code = toponym["metaDataProperty"]["GeocoderMetaData"]["Address"].get(
                "postal_code", "")
    return list(map(float, toponym_coodrinates.split(" "))), address, post_code