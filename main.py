import os
import sys

from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QPushButton, \
    QLineEdit, QCheckBox

from func import get_map, get_info
from variables import SCREEN_SIZE, notches


class Example(QMainWindow):
    def __init__(self, scale, x, y):
        super().__init__()
        self.initUI()
        self.scale = scale
        self.pos = self.x, self.y = x, y
        self.n = 0
        self.view = data[self.n]
        self.is_post_code = False

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Up and self.scale:
            self.scale *= 2
        elif event.key() == Qt.Key_Down and self.scale > 0.00001:
            self.scale /= 2
        elif event.key() == Qt.Key_A and self.x > -190 + self.scale:
            self.x -= self.scale
        elif event.key() == Qt.Key_D and self.x < 190 - self.scale:
            self.x += self.scale
        elif event.key() == Qt.Key_W and self.y < 80 - self.scale:
            self.y += self.scale
        elif event.key() == Qt.Key_S and self.y > -80 + self.scale:
            self.y -= self.scale
        self.show_map()

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton) or (event.button() == Qt.RightButton):
            self.n += 1
            self.view = data[self.n % 3]
            self.show_map()

    def setImage(self, name):
        self.pixmap = QPixmap(name)
        # Если картинки нет, то QPixmap будет пустым,
        # а исключения не будет
        # Отображаем содержимое QPixmap в объекте QLabel
        self.image.setPixmap(self.pixmap)

    def initUI(self):
        self.setGeometry(600, 450, *SCREEN_SIZE)
        self.setWindowTitle('Отображение картинки')

        ## Изображение
        self.pixmap = QPixmap('map.png')
        # Если картинки нет, то QPixmap будет пустым,
        # а исключения не будет
        self.image = QLabel(self)
        # self.image.move(80, 60)
        self.image.resize(600, 450)
        # Отображаем содержимое QPixmap в объекте QLabel
        self.image.setPixmap(self.pixmap)
        self.find_btn = QPushButton("Искать", self)
        self.find_btn.move(SCREEN_SIZE[0] - 100, SCREEN_SIZE[1] - 90)
        self.find_btn.resize(45, 27)

        self.reset = QPushButton("Сброс", self)
        self.reset.move(SCREEN_SIZE[0] - 55, SCREEN_SIZE[1] - 90)
        self.reset.resize(45, 27)

        self.output_address = QLineEdit(self)
        self.output_address.move(10, SCREEN_SIZE[1] - 60)
        self.output_address.resize(SCREEN_SIZE[0] - 115, 25)
        self.output_address.setPlaceholderText("Найденный адрес")
        self.output_address.setReadOnly(True)

        self.search_input = QLineEdit(self)
        self.search_input.move(10, SCREEN_SIZE[1] - 90)
        self.search_input.resize(SCREEN_SIZE[0] - 115, 25)
        self.search_input.setPlaceholderText("Поиск")
        self.search_input.setFocusPolicy(Qt.ClickFocus)
        # self.search_input.installEventFilter(self)
        self.find_btn.clicked.connect(self.find_on_map)
        self.reset.clicked.connect(self.reset_point)

        self.post_code = QCheckBox("почтовый индекс", self)
        self.post_code.move(SCREEN_SIZE[0] - 100, SCREEN_SIZE[1] - 60)
        self.post_code.stateChanged.connect(self.change_post_code)

    def change_post_code(self):
        self.is_post_code = not self.is_post_code
        self.find_on_map()

    def show_map(self):
        map_file = get_map(self.x, self.y, self.scale, self.view)
        self.setImage(map_file)

    def reset_point(self):
        global notches
        if notches:
            del notches[-1]
            self.output_address.setText("")
            self.show_map()

    def find_on_map(self):
        global notches
        try:
            (self.x, self.y), address, post_code = (get_info(self.search_input.text()))
            if (self.x, self.y) not in notches:
                notches += [(self.x, self.y)]
            if self.is_post_code:
                address += " " + post_code
            self.output_address.setText(address)
            self.show_map()

        except KeyError:
            print("Ничего не найдено")


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


def clean():
    for i in range(6, 30):
        os.remove(f"map{i + 1}.png")


if __name__ == '__main__':
    sys.excepthook = except_hook
    data = ['map', 'sat', 'sat,skl']
    print("вводите все корректно, через запятую долготу и широту")
    x, y = [float(i) for i in input("Координаты: ").split(",")]
    print("вводите все корректно, пример масштаб '0.009'")
    scale = float(input("Масштаб: "))

    app = QApplication(sys.argv)
    ex = Example(scale, x, y)
    ex.show()
    app.exec()
    clean()
    sys.exit()